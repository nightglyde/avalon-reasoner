import re
import uuid

from util import *
from .player import Player, ALL
from .mission import Mission

##############
#    GAME    #
##############
class Game(MyObject):
    def __init__(self, uniqueID=uuid.uuid4()):
        self.args = (uniqueID,)
        self.setData(GAME, self)

        self.numPlayers = 0
        self.numGood = 0
        self.numEvil = 0

        # Character selecting
        self.selectedCharactersGood = set()
        self.selectedCharactersEvil = set()

        # Game data
        self.characters = set()
        self.players = set()
        self.playerMap = {} # playerNum: player
        self.nameMap = {"all": ALL} # name: player
        self.missions = set()
        self.missionMap = {} # missionNum: mission

        self.gameMode = None
        self.numSuccess = 0
        self.numFail = 0
        self.missionNum = 0
        self.victoryStatus = INCOMPLETE

        self.reasoner = None

        self.onePlayer = False
        self.player = None

    #####################
    # Utility Functions #
    #####################
    def getPlayer(self, player):
        if isinstance(player, str):
            if player.lower() in self.nameMap:
                player = self.nameMap[player.lower()]
            else:
                try:
                    player = int(player)
                except ValueError:
                    raise RuntimeError
        if isinstance(player, int):
            if player in self.playerMap:
                player = self.playerMap[player]
        if isinstance(player, Player):
            if player in self.players:
                return player
        raise RuntimeError

    def getMission(self, missionNum=None):
        if missionNum != None:
            return self.missionMap[missionNum]
        return self.missionMap[self.missionNum]

    def numLoyal(self):
        characters = self.characters & goodCharacters & specialCharacters
        return self.numGood - len(characters)

    def numSpy(self):
        characters = self.characters & evilCharacters & specialCharacters
        return self.numEvil - len(characters)

    ###################
    # Set Num Players #
    ###################
    def setNumPlayers(self, numPlayers):
        self.numPlayers = numPlayers
        self.numGood, self.numEvil = players_setUp[self.numPlayers]
        print("\nGame with {} players.".format(self.numPlayers))
        print("({} good, {} evil)".format(self.numGood, self.numEvil))

        for playerNum in range(self.numPlayers):
            player = Player(self, playerNum)
            self.players.add(player)
            self.playerMap[playerNum] = player

        for missionNum in range(NUM_MISSIONS):
            mission = Mission(self, missionNum)
            self.missions.add(mission)
            self.missionMap[missionNum] = mission
        self.missionMap[5] = None

    #################
    # Set Game Mode #
    #################
    def setGameMode(self, gameMode):
        self.gameMode = gameMode
        self.selectedCharactersGood = set()
        self.selectedCharactersEvil = set()
        if self.gameMode == AVALON:
            self.characters = {LOYAL, SPY, MERLIN, ASSASSIN}
        else:
            self.characters = {LOYAL, SPY}

    ##########################
    # Select Character Cards #
    ##########################
    def addCharacter(self, character): # Avalon only
        try:
            character = getCharacter(character)
        except RuntimeError:
            print("Invaild character!")
            return
        if not character.special:
            print("{} is not a special character!".format(character))
        elif character in self.characters:
            print("Character has already been added!")

        elif character.loyalty == GOOD:
            if self.numLoyal() == 0:
                print("Too many good characters!")
            else:
                self.characters.add(character)
                if self.numLoyal() == 0:
                    self.characters.discard(LOYAL)

        elif character.loyalty == EVIL:
            if self.numSpy() == 0:
                print("Too many evil characters!")
            else:
                self.characters.add(character)
                if self.numSpy() == 0:
                    self.characters.discard(SPY)

    def clearCharacters(self): # Avalon only
        self.characters = {LOYAL, SPY, MERLIN, ASSASSIN}
        print("Cleared character cards.")

    #####################
    # Enter Player Data #
    #####################
    def setName(self, player, name):
        try:
            player = self.getPlayer(player)
        except RuntimeError:
            print("Invalid player!")
            return False

        self.nameMap[name.lower()] = player
        player.setName(name)
        return True

    def checkNamesReady(self):
        for player in self.players:
            if player.name == "":
                return False

        seenNames = set()
        for player in self.game.players:
            for i in range(1, len(player.name)):
                shortName = player.name.lower()[:i]
                if shortName in restrictedNames:
                    continue
                if shortName in self.nameMap:
                    if shortName in seenNames:
                        del self.nameMap[shortName]
                elif not shortName in seenNames:
                    self.nameMap[shortName] = player
                    seenNames.add(shortName)
        return True

    def enterMainPlayer(self, player):
        if not self.onePlayer:
            self.crash("Not one player mode!")
        try:
            player = self.getPlayer(player)
            self.player = player
            return True
        except RuntimeError:
            print("Invalid player!")
            return False

    def enterPlayerCharacter(self, character):
        if not self.onePlayer:
            self.crash("Not one player mode!")
        try:
            character = getCharacter(character)
            if character in self.characters:
                self.player.setCharacter(character)
                return True
            print("Characther not in game!")
            return False
        except RuntimeError:
            print("Invalid character!")
            return False

    ############
    # End Game #
    ############
    def tryVictory(self):
        if self.numSuccess == 3:
            self.victoryStatus = LOYAL_VICTORY
            return True
        if self.numFail == 3:
            self.victoryStatus = SPY_VICTORY
            return True
        if self.getMission().suggestionNum > 4:
            self.victoryStatus = SPY_VICTORY
            return True
        return False

    def merlinGuess(self, merlin, guess):
        if merlin == guess:
            print("Correct! {} is Merlin.".format(merlin))
            self.victoryStatus = SPY_VICTORY
        else:
            print("Incorrect! {} is Merlin.".format(merlin))

    def endGame(self):
        if self.victoryStatus == LOYAL_VICTORY:
            print("\nLoyal victory!")
        elif self.victoryStatus == SPY_VICTORY:
            print("\nSpy victory!")
        else:
            self.crash("Invalid victoryStatus!")

    #########
    # Crash #
    #########
    def crash(self, message):
        print("\nCRASH!")
        print(message)
        input("Press enter to print all data and exit program.")
        print("\nGame:")
        self.printData()
        print("\nPlayers:")
        for player in self.players:
            print(player)
            player.printData()
        print("\nNicknames:")
        for name in self.nameMap:
            print(name, self.nameMap[name])
        print("\nCharacters:")
        for character in self.characters:
            print(character)
        print("\nMissions:")
        for mission in self.missions:
            print(mission)
            mission.printData()
        if self.reasoner != None:
            print("\nReasoner:")
            self.reasoner.printData()
        raise Exception(message)

    ###################
    # Special Methods #
    ###################
    def printCharacterOptions(self):
        print()
        print("Characters in game:", nice(self.characters))
        characterOptions = characters - self.characters
        if self.numLoyal() == 0:
            characterOptions -= goodCharacters
        if self.numSpy() == 0:
            characterOptions -= evilCharacters
        print("Options:", nice(characterOptions))

    def printMissionHistory(self):
        if self.missionNum == 0:
            return
        print("\nHISTORY:")
        for mission in range(self.missionNum):
            print("Mission {}:".format(mission))
            mission = self.getMission(mission)
            mission.printHistory()
        mission = self.getMission()
        if mission == None:
            return
        if mission.suggestionNum > 0:
            mission.printHistory()

    def printData(self):
        print()
        print(self.gameMode)
        print("{} players ({} good, {} evil)".format(self.numPlayers,
                                                     self.numGood,
                                                     self.numEvil))
        print("Characters:", nice(self.characters))
        print("Players:", nice(self.players))
        print("Missions:", nice(self.missions))

    def __str__(self):
        s = ""
        s += "{}".format(self.gameMode)
        s += ":{}".format(self.getMission())
        s += ":players={}".format(self.numPlayers)
        s += ":success={}".format(self.numSuccess)
        s += ":failure={}".format(self.numFail)
        return s

