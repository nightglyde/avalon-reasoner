import re

from util import *

class Mission(MyObject):
    def __init__(self, game, num):
        self.args = (game, num)
        self.setData(MISSION, game)

        self.num = num

        # Mission parameters
        missionParameters = missions_setUp[self.game.numPlayers][self.num]
        self.numPlayers = missionParameters[0]
        self.failCondition = missionParameters[1]

        # Set up mission
        self.suggestionNum = 0
        self.leader = None
        self.players = set()
        self.approves = set()
        self.rejects = set()
        self.rejectedMissions = []

        # Mission results
        self.numFails = None

    #####################
    # Utility Functions #
    #####################
    def getResult(self):
        if self.numFails == None:
            return None
        if self.numFails < self.failCondition:
            return SUCCESS
        else:
            return FAIL

    #################
    # Mission Setup #
    #################
    def setLeader(self, leader):
        self.leader = leader

    ###################
    # Suggest Players #
    ###################
    def clearPlayers(self):
        self.players = set()
        print("Cleared suggested players.")

    def addPlayer(self, player):
        self.players.add(player)

    ###############
    # Enter Votes #
    ###############
    def clearVotes(self):
        self.approves = set()
        self.rejects = set()
        print("Cleared votes.")

    def addVote(self, player, vote):
        if vote == APPROVE:
            self.approves.add(player)
        elif vote == REJECT:
            self.rejects.add(player)
        else:
            self.game.crash("Vote not approve or reject!")

    def missionApproved(self):
        numApproves = len(self.approves)
        numRejects = len(self.rejects)
        print("\n{}:{}".format(numApproves, numRejects))
        self.game.reasoner.recordVotes(self)
        if numApproves > numRejects:
            print("Mission approved.")
            return True
        else:
            self.rejectedMissions.append((self.leader, self.players,
                                          self.approves, self.rejects))
            self.suggestionNum += 1
            print("Mission rejected.")
            return False

    #################
    # Enter Results #
    #################
    def enterResults(self, numFails):
        self.numFails = numFails
        if self.getResult() == SUCCESS:
            self.game.numSuccess += 1
            print("Mission succeeded!")
        elif self.getResult() == FAIL:
            self.game.numFail += 1
            print("Mission failed!")
            self.game.reasoner.missionFailed(self)
        else:
            self.game.crash("Mission is not SUCCESS or FAIL!")
        self.game.missionNum += 1

    ###################
    # Special Methods #
    ###################
    def teamString(self, leader, players, approves, rejects):
        s = "    {}'s Team:".format(leader)
        s += "\n      Players:  {}".format(nice(players))
        s += "\n      Approves: {}".format(nice(approves))
        s += "\n      Rejects:  {}".format(nice(rejects))
        return s

    def printHistory(self):
        if len(self.rejectedMissions) > 0:
            for leader, players, approves, rejects in self.rejectedMissions:
                print(self.teamString(leader, players, approves, rejects))
        if self.numFails != None:
            print(self.teamString(self.leader, self.players,
                                  self.approves, self.rejects))
            print("    Fails: {}/{}".format(self.numFails, self.failCondition))

    def printData(self):
        print("\nMission {}".format(self.num))
        print("Leader: {}".format(self.leader))
        print("Players on mission: {}".format(nice(self.players)))
        print("Votes: approves={}, rejects={}".format(len(self.approves),
                                                      len(self.rejects)))
        if self.numFails != None:
            if self.getResult() == SUCCESS:
                print("Success")
            elif self.getResult() == FAIL:
                print("Fail")
            else:
                self.game.crash("Is not success or failure!")
            print("{}/{} fails".format(self.numFails, self.failCondition))
        if len(self.rejectedMissions) > 0:
            print("Rejected Missions:")
            for leader, players, approves, rejects in self.rejectedMissions:
                print(self.teamString(leader, players, approves, rejects))

    def __str__(self):
        s = "mission{}:suggestion{}".format(self.num, self.suggestionNum)
        s += ":{}".format(self.leader)
        s += ":[{}]".format(nice(self.players))
        if self.numFails != None:
            if self.getResult() == SUCCESS:
                s += ":Success"
            elif self.getResult() == FAIL:
                s += ":Fail"
            else:
                self.game.crash("Neither success nor failure!")
        return s

