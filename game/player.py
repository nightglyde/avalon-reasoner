from util import *

class Player(MyObject):
    def __init__(self, game, num):
        self.args = (game, num)
        self.setData(PLAYER, game)

        self.num = num

        # Player details
        self.name = ""
        self.nameKey = ""
        self.character = None
        self.loyalty = None

    ############
    # Add Data #
    ############
    def setName(self, name):
        self.name = name
        self.nameKey = self.name[:5].lower()

    def setCharacter(self, character):
        self.character = character
        self.loyalty = character.loyalty

    def setLoyalty(self, loyalty):
        if self.character != None and self.character.loyalty != loyalty:
            self.game.crash("Loyalty inconsistent with player's character.")
        self.loyalty = loyalty

    ###################
    # Special Methods #
    ###################
    def printData(self):
        print("num:", self.num)
        print("name:", self.name)
        print("character:", self.character)
        print("loyalty:", self.loyalty)

    def short(self):
        s = self.name[:5]
        return " "*(5-len(s)) + s + ":"

    def __repr__(self):
        return self.nameKey

    def __str__(self):
        s = self.name
        if self.character != None:
            s += str(self.character)
        elif self.loyalty != None:
            s += str(self.loyalty)
        return s

class AllPlayers(Player):
    def __init__(self):
        Player.__init__(self, None, None)
        Player.setName(self, "All")

    def setName(self, name):
        raise NotImplemented

    def setCharacter(self, character):
        raise NotImplemented

    def setLoyalty(self, loyalty):
        raise NotImplemented

ALL = AllPlayers()

