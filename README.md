# [Knowledge and Avalon](https://bitbucket.org/nightglyde/avalon-reasoner/wiki/Home)
*Using Dynamic Epistemic Logic to Model the Game of Avalon*

***

## How to Run
* Install [Python 3](https://www.python.org/downloads/)
* Install [PyKE](https://sourceforge.net/projects/pyke/)
* Run start.py

***

## [Dynamic Epistemic Logic](https://bitbucket.org/nightglyde/avalon-reasoner/wiki/Dynamic%20Epistemic%20Logic)
*Overview of dynamic epistemic logic, and how intelligent agents can solve logic puzzles.*

***

## [Modelling Avalon](https://bitbucket.org/nightglyde/avalon-reasoner/wiki/Avalon)
*Rules of Avalon, and how the game's logic can be modelled using dynamic epistemic logic.*

***

## [Implementation](https://bitbucket.org/nightglyde/avalon-reasoner/wiki/Implementation)
*How to implement this logic in code, and provide meaningful information about the game state.*