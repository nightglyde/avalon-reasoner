from .util import *
from .myObject import MyObject

loyaltyNames = {GOOD_NUM: "Good", EVIL_NUM: "Evil"}

abbreviations = {GOOD_NUM: "G", EVIL_NUM: "E"}

class Loyalty(MyObject):
    def __init__(self, num):
        self.args = (num,)
        self.setData(LOYALTY, None)

        self.num = num
        self.name = loyaltyNames[self.num]

        self.abbr = abbreviations[self.num]

    def __repr__(self):
        return self.name

GOOD = Loyalty(GOOD_NUM)
EVIL = Loyalty(EVIL_NUM)

loyalties = {GOOD, EVIL}

loyaltyMap = {GOOD_NUM: GOOD, EVIL_NUM: EVIL}
loyaltyNums = {"good": GOOD_NUM, "evil": EVIL_NUM}

from .character import Character, characterMap
def getLoyalty(loyalty):
    if isinstance(loyalty, str):
        if loyalty.lower() in loyaltyNums:
            loyalty = loyaltyNums[loyalty.lower()]
        elif loyalty.lower() in characterMap:
            loyalty = characterMap[loyalty.lower()].loyalty
        else:
            try:
                loyalty = int(loyalty)
            except ValueError:
                raise RuntimeError
    if isinstance(loyalty, int):
        if loyalty in loyaltyMap:
            loyalty = loyaltyMap[loyalty]
        elif loyalty in characterMap:
            loyalty = characterMap[loyalty]
    if isinstance(loyalty, Character):
        loyalty = loyalty.loyalty
    if isinstance(loyalty, Loyalty):
        if loyalty in loyalties:
            return loyalty
    raise RuntimeError

