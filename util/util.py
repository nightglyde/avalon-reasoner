PLAYERS_LOWER_LIMIT = 5
PLAYERS_UPPER_LIMIT = 10
players_setUp = {
    5: (3, 2),  6: (4, 2),  7: (4, 3),
    8: (5, 3),  9: (6, 3), 10: (6, 4),
}

AVALON = "Avalon"
RESISTANCE = "Resistance"

NUM_MISSIONS = 5
missions_setUp = {
    5: ((2,1), (3,1), (2,1), (3,1), (3,1)),
    6: ((2,1), (3,1), (4,1), (3,1), (4,1)),
    7: ((2,1), (3,1), (3,1), (4,2), (4,1)),
    8: ((3,1), (4,1), (4,1), (5,2), (5,1)),
    9: ((3,1), (4,1), (4,1), (5,2), (5,1)),
    10:((3,1), (4,1), (4,1), (5,2), (5,1)),
}

restrictedNames = { # change to lower case before checking against this
"loyal", "merlin", "percival", "good", "loyalist", "loyalists",
"spy", "assassin", "morgana", "mordred", "oberon", "bad", "spies",
"game", "player", "character", "nickname", "mission",
"playernum", "characternum", "missionnum",
"accept", "accepted", "approve", "approved", "reject", "rejected",
"success", "successful", "succeeded", "fail", "failure", "failed",
"play", "start", "end", "clear", "exit", "quit", "analysis",
"hascharacter", "hasloyalty", "knows", "canimagine",
"and", "or", "implies", "iff", "all", "current",
}

NUM_TESTS = 5

# Knowledge Engine
ruleDirectory = "reasoner/rules/"
ruleFile = "r"

# Loyalty Nums
GOOD_NUM = 1
EVIL_NUM = 2

# Player Nums
LOYAL_NUM            = 11
MERLIN_NUM           = 12
PERCIVAL_NUM         = 13
SPY_NUM              = 21
ASSASSIN_NUM         = 22
MORGANA_NUM          = 23
MORDRED_NUM          = 24
OBERON_NUM           = 25

# Voting
APPROVE = 31
REJECT  = 32

# Mission Cards
SUCCESS = 41
FAIL    = 42

# Victory Status
INCOMPLETE    = 50
LOYAL_VICTORY = 51
SPY_VICTORY   = 52

# Classes
CHARACTER     = (0, "Character")
LOYALTY       = (1, "Loyalty")
GAME          = (2, "Game")
PLAYER        = (3, "Player")
MISSION       = (4, "Mission")
REASONER      = (5, "Reasoner")
KNOWLEDGE     = (6, "Knowledge")
PROBABILITIES = (7, "Probabilities")
MODEL         = (8, "Model")

def nice(items):
    if isinstance(items, set):
        items = sorted(items)
    s = ""
    for item in items:
        if not isinstance(item, str):
            item = repr(item)
        s += item + ", "
    return s[:-2]

