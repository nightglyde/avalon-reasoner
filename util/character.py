from .util import *
from .myObject import MyObject
from .loyalty import Loyalty, GOOD, EVIL

characterName = {
    LOYAL_NUM:   "Loyal",   MERLIN_NUM:   "Merlin",   PERCIVAL_NUM: "Percival",
    SPY_NUM:     "Spy",     ASSASSIN_NUM: "Assassin", MORGANA_NUM:  "Morgana",
    MORDRED_NUM: "Mordred", OBERON_NUM:   "Oberon",
}

characterSpecial = {
    LOYAL_NUM:   False, MERLIN_NUM:   True, PERCIVAL_NUM: True,
    SPY_NUM:     False, ASSASSIN_NUM: True, MORGANA_NUM:  True,
    MORDRED_NUM: True,  OBERON_NUM:   True,
}

characterLoyalty = {
    LOYAL_NUM:    GOOD, MERLIN_NUM:  GOOD, PERCIVAL_NUM: GOOD, SPY_NUM:    EVIL,
    ASSASSIN_NUM: EVIL, MORGANA_NUM: EVIL, MORDRED_NUM:  EVIL, OBERON_NUM: EVIL,
}

characterAbbr = {
    LOYAL_NUM:    "L", MERLIN_NUM:  "M", PERCIVAL_NUM: "P", SPY_NUM:    "S",
    ASSASSIN_NUM: "A", MORGANA_NUM: "R", MORDRED_NUM:  "D", OBERON_NUM: "O",
}

class Character(MyObject):
    def __init__(self, num):
        self.args = (num,)
        self.setData(CHARACTER, None)

        self.num     = num
        self.name    = characterName[self.num]
        self.special = characterSpecial[self.num]
        self.loyalty = characterLoyalty[self.num]
        self.abbr    = characterAbbr[self.num]

    def __repr__(self):
        return self.name

LOYAL    = Character(LOYAL_NUM)
MERLIN   = Character(MERLIN_NUM)
PERCIVAL = Character(PERCIVAL_NUM)
SPY      = Character(SPY_NUM)
ASSASSIN = Character(ASSASSIN_NUM)
MORGANA  = Character(MORGANA_NUM)
MORDRED  = Character(MORDRED_NUM)
OBERON   = Character(OBERON_NUM)

characters = {LOYAL, MERLIN, PERCIVAL, SPY, ASSASSIN, MORGANA, MORDRED, OBERON}
goodCharacters = {LOYAL, MERLIN, PERCIVAL}
evilCharacters = {SPY, ASSASSIN, MORGANA, MORDRED, OBERON}
specialCharacters = {MERLIN, PERCIVAL, ASSASSIN, MORGANA, MORDRED, OBERON}

characterMap = {
    "loyal":   LOYAL,   "merlin":   MERLIN,   "percival": PERCIVAL,
    "spy":     SPY,     "assassin": ASSASSIN, "morgana":  MORGANA,
    "mordred": MORDRED, "oberon":   OBERON,

    LOYAL_NUM:   LOYAL,   MERLIN_NUM:   MERLIN,   PERCIVAL_NUM: PERCIVAL,
    SPY_NUM:     SPY,     ASSASSIN_NUM: ASSASSIN, MORGANA_NUM:  MORGANA,
    MORDRED_NUM: MORDRED, OBERON_NUM:   OBERON,

    "s": SPY,        "sp": SPY,
    "l": LOYAL,      "lo": LOYAL,    "loy": LOYAL,    "loya": LOYAL,
    "o": OBERON,     "ob": OBERON,   "obe": OBERON,   "ober": OBERON,
    "p": PERCIVAL,   "pe": PERCIVAL, "per": PERCIVAL, "perc": PERCIVAL,
    "a": ASSASSIN,   "as": ASSASSIN, "ass": ASSASSIN, "assa": ASSASSIN,
    "merli": MERLIN, "me": MERLIN,   "mer": MERLIN,   "merl": MERLIN,
    "obero": OBERON,                                  "morg": MORGANA,
    "morga": MORGANA,  "morgan": MORGANA,             "mord": MORDRED,
    "mordr": MORDRED,  "mordre": MORDRED,
    "perci": PERCIVAL, "perciv": PERCIVAL, "perciva": PERCIVAL,
    "assas": ASSASSIN, "assass": ASSASSIN, "assassi": ASSASSIN,
}

def getCharacter(character):
    if isinstance(character, str):
        if character.lower() in characterMap:
            return characterMap[character.lower()]
        else:
            try:
                character = int(character)
            except ValueError:
                raise RuntimeError
    if isinstance(character, int):
        if character in characterMap:
            return characterMap[character]
    if isinstance(character, Character):
        if character in characters:
            return character
    raise RuntimeError

