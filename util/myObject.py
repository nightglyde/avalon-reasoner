class MyObject:
    def setData(self, classData, game):
        self.order = classData[0]
        self.text = classData[1]
        self.game = game
        self.keepHash = None

    def __hash__(self):
        if self.keepHash != None:
            return self.keepHash
        self.keepHash = hash(self.order) ^ hash(self.text) ^ hash(self.args)
        return self.keepHash

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.args == other.args

    def __lt__(self, other):
        if issubclass(other.__class__, MyObject):
            if self.order == other.order:
                return self.args < other.args
            return self.order < other.order
        if isinstance(other, str):
            return True

    def __repr__(self):
        args = tuple(self.args)
        if isinstance(args, tuple):
            return "{}{}".format(self.text, args)
        else:
            return "{}({})".format(self.text, args)

