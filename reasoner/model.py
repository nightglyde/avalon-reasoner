from util import *
from game import ALL

class Model(MyObject):
    def __init__(self, reasoner, player, character, notCharacters, hidden=False):
        self.args = (reasoner, player, character, notCharacters, hidden)
        self.setData(MODEL, reasoner.game)

        self.reasoner = reasoner
        self.scopes = self.game.players
        self.player = player
        self.character = character
        self.notCharacters = notCharacters
        self.hidden = hidden

        self.notChars = ""
        for character in self.notCharacters:
            self.notChars += character.abbr.lower()

        self.reasoner.engine.assert_("k", "model", (str(self),))

        if self.character in {GOOD, EVIL}:
            self.reasoner.addFact("player_loyalty", self.player,
                                  self.character, self.player, self)
            for character in self.notCharacters:
                self.reasoner.addFact("player_not_character", self.player,
                                      character, self.player, self)
        elif self.character in self.game.characters:
            self.reasoner.addFact("player_character", self.player,
                                  self.character, self.player, self)
        elif self.character == EVIL_SANS_OBERON:
            self.reasoner.addFact("evil_saw", self.player, "Yes",
                                  self.player, self)

        if self.hidden or not self.game.onePlayer:
            return
        self.reasoner.engine.assert_("k", "model_inherits",
                                     (str(self), str(CURRENT)))
        if self.character in {GOOD, EVIL}:
            self.reasoner.addFact("player_loyalty", self.player,
                                  self.character, self.game.player, self)
            for character in self.notCharacters:
                self.reasoner.addFact("player_not_character", self.player,
                                      character, self.game.player, self)
        elif self.character in self.game.characters:
            self.reasoner.addFact("player_character", self.player,
                                  self.character, self.game.player, self)
        elif self.character == EVIL_SANS_OBERON:
            self.reasoner.addFact("evil_saw", self.player, "Yes",
                                  self.game.player, self)

    def title(self):
        s = "if {} is {}".format(self.player, self.character)
        if len(self.notCharacters) > 0:
            s += " (sans "
            for character in sorted(self.notCharacters):
                s += "{}, ".format(character)
            s = s[:-2] + ")"
        return s 

    def __str__(self):
        if self.hidden:
            return "{}{}hidden".format(repr(self.player), self.character.abbr)
        return "{}{}{}".format(repr(self.player),
                               self.character.abbr,
                               self.notChars)

class CurrentModel(Model):
    def __init__(self):
        self.args = (None, None, None)
        self.setData(MODEL, None)

        self.scopes = {ALL}

    def title(self):
        return "Current"

    def __str__(self):
        return "Current"

CURRENT = CurrentModel()

