import itertools

from util import *

class Probabilities(MyObject):
    def __init__(self, reasoner, parent=None):
        self.args = (reasoner, parent)
        self.setData(PROBABILITIES, reasoner.game)

        self.reasoner = reasoner

        self.worlds = self.reasoner.worlds.copy()

        self.playerWorlds = {}
        for player in self.game.players:
            self.playerWorlds[player] = set()
        for world in self.worlds:
            for player in world:
                self.playerWorlds[player].add(world)

    def setLoyalty(self, player, loyalty):
        if loyalty == GOOD:
            self.worlds -= self.playerWorlds[player]
        elif loyalty == EVIL:
            self.worlds = self.playerWorlds[player].copy()

        for player in self.game.players:
            self.playerWorlds[player] &= self.worlds

    def removeWorlds(self, players, evilRange):
        players = set(players)
        for numEvil in evilRange:
            for combination in itertools.combinations(players, numEvil):
                evilPlayers = set(combination)
                goodPlayers = players - evilPlayers
                worlds = self.worlds.copy()
                for world in worlds:
                    count = 0
                    for evilPlayer in world:
                        if evilPlayer in goodPlayers:
                            break
                        if evilPlayer in evilPlayers:
                            count += 1
                    else:
                        if count == numEvil:
                            self.worlds.discard(world)

        for player in self.game.players:
            self.playerWorlds[player] &= self.worlds

    def playerProbability(self, player):
        if len(self.worlds) == 0:
            return None
        return len(self.playerWorlds[player]) / len(self.worlds)

    def averageTeam(self, team):
        cumSpies = 0
        lower = -1
        upper = -1
        for world in self.worlds:
            if (world, team) in self.reasoner.teamSpiesDP:
                spies = self.reasoner.teamSpiesDP[(world, team)]
            else:
                spies = 0
                for player in world:
                    if player in team:
                        spies += 1
                self.reasoner.teamSpiesDP[(world, team)] = spies
            if spies < lower or lower == -1:
                lower = spies
            if spies > upper or upper == -1:
                upper = spies 
            cumSpies += spies
        return lower, (cumSpies / len(self.worlds)), upper

