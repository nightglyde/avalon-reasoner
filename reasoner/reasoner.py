import itertools
from pyke import knowledge_engine

from util import *
from game import ALL
from .probabilities import Probabilities
from .model import Model, CURRENT

############
# REASONER #
############
class Reasoner(MyObject):
    def __init__(self, game):
        self.args = (game,)
        self.setData(REASONER, game)

        self.seenPlayers = set()

        self.engine = knowledge_engine.engine(ruleDirectory)
        self.engine.activate(ruleFile)

        if self.game.onePlayer:
            CURRENT.scopes = self.game.players
        else:
            self.engine.assert_("k", "scope", (repr(ALL),))

        self.models = {CURRENT}
        self.playerModels = {}
        for player in self.game.players:
            self.playerModels[player] = set()
        self.hiddenModels = {}

        self.playerLoyalty = set()
        self.percivalPair = set()

        self.worlds = set()
        for combination in itertools.combinations(sorted(self.game.players),
                                                  self.game.numEvil):
            self.worlds.add(tuple(combination))
        self.teamSpiesDP = {}

        self.probs = {}
        for scope in CURRENT.scopes:
            self.probs[(CURRENT, scope)] = Probabilities(self)

        for character in characters - self.game.characters:
            self.engine.assert_("k", "not_character", (str(character),))
        for player in self.game.players:
            self.engine.assert_("k", "scope", (repr(player),))
            self.engine.assert_("k", "player", (repr(player),))

        if self.game.onePlayer:
            self.addFact("player_character",
                         self.game.player,
                         self.game.player.character,
                         self.game.player)
            if self.game.player.character in {SPY, ASSASSIN, MORGANA, MORDRED}:
                self.seenPlayers.add(self.game.player)

        self.teamVotes = []
        self.loyalKnows = {}

    ###################
    # Utility Methods #
    ###################
    def query(self, query):
        try:
            v, plan = self.engine.prove_1_goal(query)
            if not v:
                return True
            return v
        except knowledge_engine.CanNotProve:
            return False

    ####################
    # Refine Knowledge #
    ####################
    def trimModels(self):
        models = set()
        for model in self.models:
            if not self.query(
                "r.contradiction({}, $scope, $details)".format(model)):
                models.add(model)
            else:
                print("Remove model {}".format(model))
        self.models = models

    def _checkLoyalties(self, player, scope, model):
        v = self.query("k.player_loyalty(({}, {}), {}, $result)".format(
                       model, repr(scope), repr(player)))
        if v:
            result = getLoyalty(v['result'])
            self.probs[(model, scope)].setLoyalty(player, result)
            self.playerLoyalty.add((model, scope, player))
            if model == CURRENT:
                player.setLoyalty(result)
            return True
        return False

    def checkLoyalties(self):
        changes = 0
        for model in self.models:
            for scope in model.scopes:
                for player in self.game.players:
                    if not (model, scope, player) in self.playerLoyalty:
                        if self._checkLoyalties(player, scope, model):
                            changes += 1
        return changes

    def _checkProbabilities(self, player, scope, model):
        prob = self.playerSpyProbability(player, scope, model)
        if prob == 0.0:
            self.addFact("player_loyalty", player, GOOD, scope, model)
            self.playerLoyalty.add((model, scope, player))
            if model == CURRENT:
                player.setLoyalty(GOOD)
            return True
        if prob == 1.0:
            self.addFact("player_loyalty", player, EVIL, scope, model)
            self.playerLoyalty.add((model, scope, player))
            if model == CURRENT:
                player.setLoyalty(EVIL)
            return True
        return False

    def checkProbabilities(self):
        changes = 0
        for model in self.models:
            for scope in model.scopes:
                for player in self.game.players:
                    if not (model, scope, player) in self.playerLoyalty:
                        if self._checkProbabilities(player, scope, model):
                            changes += 1
        return changes

    def _checkPercivalPair(self, scope, model):
        v = self.query("r.percival_pair(({}, {}), $player1, $player2)".format(
                       model, repr(scope)))
        if v:
            player1 = self.game.getPlayer(v['player1'])
            player2 = self.game.getPlayer(v['player2'])
            self.probs[(model, scope)].removeWorlds({player1, player2}, [0, 2])
            self.percivalPair.add((model, scope))
            return True
        return False

    def checkPercivalPair(self):
        changes = 0
        for model in self.models:
            for scope in model.scopes:
                if not (model, scope) in self.percivalPair:
                    if self._checkPercivalPair(scope, model):
                        changes += 1
        return changes

    def checkKnowledge(self):
        changes = 0
        self.trimModels()
        changes += self.checkProbabilities()
        changes += self.checkLoyalties()
        if MORGANA in self.game.characters:
            changes += self.checkPercivalPair()
        print(changes, "changes.")
        return changes > 0

    def refineKnowledge(self):
        print("\nRefining knowledge...")
        while self.checkKnowledge():
            pass
        print("Complete.")

    ####################
    # Knowledge Engine #
    ####################
    def addFact(self, identifier, player, result, scope, model=CURRENT):
        if not isinstance(model, str):
            model = str(model)
        if not isinstance(scope, str):
            scope = repr(scope)
        if not isinstance(player, str):
            player = repr(player)
        if not isinstance(result, str):
            result = str(result)
        self.engine.assert_("k", identifier, ((model, scope), player, result))

    def createHiddenModel(self, player):
        model = Model(self, player, LOYAL, tuple(), True)
        self.models.add(model)

        self.hiddenModels[player] = model

        for scope in model.scopes:
            self.probs[(model, scope)] = Probabilities(self)
        print("Created hidden model {}".format(model))

    def createModel(self, player, character, notCharacters=set()):
        notCharacters &= self.game.characters
        notCharacters = tuple(sorted(notCharacters))
        model = Model(self, player, character, notCharacters)
        self.models.add(model)
        self.playerModels[player].add(model)
        for scope in model.scopes:
            self.probs[(model, scope)] = Probabilities(self)
        print("Created model {}".format(model))

    def generateModels(self):
        self.refineKnowledge()
        print("\nGenerating models...")
        for player in self.game.players:
            self.createHiddenModel(player)

        if not self.game.onePlayer:
            for player in self.game.players:
                if MERLIN in self.game.characters:
                    self.createModel(player, LOYAL)
                    self.createModel(player, MERLIN)
                    if PERCIVAL in self.game.characters:
                        self.createModel(player, PERCIVAL)
                else:
                    self.createModel(player, GOOD)
                if OBERON in self.game.characters:
                    self.createModel(player, OBERON)
                    self.createModel(player, EVIL, {OBERON})
                else:
                    self.createModel(player, EVIL)

        elif self.game.player.character == LOYAL:
            for player in self.game.players - {self.game.player}:
                if MERLIN in self.game.characters:
                    self.createModel(player, LOYAL)
                    self.createModel(player, MERLIN)
                    if PERCIVAL in self.game.characters:
                        self.createModel(player, PERCIVAL)
                else:
                    self.createModel(player, GOOD)
                if OBERON in self.game.characters:
                    self.createModel(player, OBERON)
                    self.createModel(player, EVIL, {OBERON})
                else:
                    self.createModel(player, EVIL)

        elif self.game.player.character == MERLIN:
            if OBERON in self.game.characters:
                for player in self.seenPlayers:
                    self.createModel(player, EVIL, {OBERON, MORDRED})
                    self.createModel(player, OBERON)
            for player in self.game.players-self.seenPlayers-{self.game.player}:
                if PERCIVAL in self.game.characters:
                    self.createModel(player, LOYAL)
                    self.createModel(player, PERCIVAL)
                    if MORDRED in self.game.characters:
                        self.createModel(player, MORDRED)
                elif MORDRED in self.game.characters:
                    self.createModel(player, LOYAL)
                    self.createModel(player, MORDRED)

        elif self.game.player.character == PERCIVAL:
            if MORGANA in self.game.characters:
                for player in self.seenPlayers:
                    self.createModel(player, MERLIN)
                    self.createModel(player, MORGANA)
            for player in self.game.players-self.seenPlayers-{self.game.player}:
                self.createModel(player, LOYAL)
                self.createModel(player, EVIL, {MORGANA})

        elif self.game.player.character in {SPY, ASSASSIN, MORGANA, MORDRED}:
            if MERLIN in self.game.characters:
                for player in self.game.players - self.seenPlayers:
                    self.createModel(player, LOYAL)
                    self.createModel(player, MERLIN)
                    if PERCIVAL in self.game.players:
                        self.createModel(player, PERCIVAL)
                    if OBERON in self.game.players:
                        self.createModel(player, OBERON)

        elif self.game.player.character == OBERON:
            for player in self.game.players - {self.game.player}:
                if MERLIN in self.game.characters:
                    self.createModel(player, LOYAL)
                    self.createModel(player, MERLIN)
                    if PERCIVAL in self.game.players:
                        self.createModel(player, PERCIVAL)
                else:
                    self.createModel(player, GOOD)
                self.createModel(player, EVIL, {OBERON})
        print("Complete.")

    #########################
    # Add Initial Knowledge #
    #########################
    def addSeenPlayer(self, player):
        try:
            player = self.game.getPlayer(player)
            if player == self.game.player:
                print("Player can't be you!")
                return False
            self.seenPlayers.add(player)
            return True
        except RuntimeError:
            print("Invalid player!")
            return False

    def checkReady(self):
        if self.game.player.character == MERLIN:
            num = self.game.numEvil
            if MORDRED in self.game.characters:
                num -= 1
        elif self.game.player.character == PERCIVAL:
            num = 1
            if MORGANA in self.game.characters:
                num += 1
        elif self.game.player.character in {SPY, ASSASSIN, MORGANA, MORDRED}:
            num = self.game.numEvil
            if OBERON in self.game.characters:
                num -= 1
        else:
            num = 0

        if len(self.seenPlayers) > num:
            self.game.crash("Too many players seen!")
        return len(self.seenPlayers) == num

    def finaliseInitialKnowledge(self):
        if self.game.player.character == MERLIN:
            for player in self.seenPlayers:
                self.addFact("merlin_saw", player, "Yes", self.game.player)
            for player in self.game.players - self.seenPlayers:
                self.addFact("merlin_saw", player, "No", self.game.player)
        elif self.game.player.character == PERCIVAL:
            for player in self.seenPlayers:
                self.addFact("percival_saw", player, "Yes", self.game.player)
            for player in self.game.players - self.seenPlayers:
                self.addFact("percival_saw", player, "No", self.game.player)
        elif self.game.player.character in {SPY, ASSASSIN, MORGANA, MORDRED}:
            for player in self.seenPlayers:
                self.addFact("evil_saw", player, "Yes", self.game.player)
            for player in self.game.players - self.seenPlayers:
                self.addFact("evil_saw", player, "No", self.game.player)

    ############################
    # Add Additional Knowledge #
    ############################
    def addMissionResult(self, team, numFails):
        if numFails == len(team):
            for player in team:
                for model in self.models:
                    for scope in model.scopes:
                        self.probs[(model, scope)].setEvil(player)
                        self.addFact("player_loyalty",
                                     player, EVIL, scope, model)
                        self.playerLoyalty.add((model, scope, player))
                player.setLoyalty(EVIL)
            return
        for model in self.models:
            for scope in model.scopes:
                self.probs[(model, scope)].removeWorlds(team, range(numFails))

    def playedFail(self, mission, playedFail):
        otherPlayers = mission.players.copy()
        otherPlayers.remove(self.game.player)
        if playedFail:
            numFails = mission.numFails - 1
        else:
            numFails = mission.numFails
        self.addMissionResult(otherPlayers, numFails)

    def missionFailed(self, mission):
        if mission.numFails > mission.numPlayers:
            self.game.crash("Impossible number of fails!")
        elif mission.numFails > self.game.numEvil:
            self.game.crash("Impossible number of fails!")
        elif mission.numFails > 0:
            self.addMissionResult(mission.players, mission.numFails)

    def recordVotes(self, mission):
        team = tuple(sorted(mission.players))
        approves = mission.approves.copy()
        self.teamVotes.append((team, mission.failCondition, approves))
        for player in self.game.players:
            model = self.hiddenModels[player]
            if model in self.models:
                lower, average, upper = self.averageTeam(team, player,
                                                         model, True)
                self.loyalKnows[(team, player)] = (lower, average, upper)

    ############
    # Get Data #
    ############
    def playerSpyProbability(self, player, scope=None, model=CURRENT):
        if scope == None:
            if self.game.onePlayer:
                scope = self.game.player
            else:
                scope = ALL
        prob = self.probs[(model, scope)].playerProbability(player)
        if prob == None:
            return 0.0
        return prob

    def getPossibleCharacters(self, player, scope, model=CURRENT):
        if not isinstance(scope, str):
            scope = repr(scope)
        v = self.query("k.player_character(({}, {}), {}, $character)".format(
                       model, scope, repr(player)))
        if v:
            character = getCharacter(v['character'])
            if model == CURRENT:
                player.setCharacter(character)
            return {character}
        notCharacters = set()
        for character in self.game.characters:
            if self.query("k.player_not_character(({}, {}), {}, {})".format(
                          model, scope, repr(player), character)):
                notCharacters.add(character)
        possibleCharacters = self.game.characters - notCharacters
        if len(possibleCharacters) == 0:
            self.game.crash("No characters possible!")
        return possibleCharacters

    def averageTeam(self, team, scope=None, model=CURRENT, preSorted=False):
        if scope == None:
            if self.game.onePlayer:
                scope = self.game.player
            else:
                scope = ALL
        if not preSorted:
            team = tuple(sorted(team))
        return self.probs[(model, scope)].averageTeam(team)

    def getSuspicion(self, player):
        votingPattern = {MERLIN: 0, GOOD: 0, EVIL: 0, SPY: 0}
        model = self.hiddenModels[player]
        if not model in self.models:
            return "{} Evil".format(player.short())
        for team, failCondition, approves in self.teamVotes:
            increaseScore = set()
            lower0, average0, upper0 = self.loyalKnows[(team, player)]
            lower1, average1, upper1 = self.averageTeam(team, player,
                                                        model, True)
            if upper1 < failCondition: # good team
                if upper0 < failCondition: # loyal knew it was good team
                    if player in approves:
                        increaseScore.add(GOOD)
                    else:
                        increaseScore.add(EVIL)
                else:
                    if player in approves:
                        increaseScore.add(MERLIN)
                    else:
                        increaseScore.add(SPY)
            elif lower1 >= failCondition: # bad team
                if lower0 >= failCondition: # loyal knew it was bad team
                    if player in approves:
                        increaseScore.add(EVIL)
                    else:
                        increaseScore.add(GOOD)
                else:
                    if player in approves: # loyal didn't know it was bad team
                        increaseScore.add(SPY)
                    else:
                        increaseScore.add(MERLIN)
            for character in increaseScore:
                if character in votingPattern:
                    votingPattern[character] += 1
        s = "\n{}".format(player.short())
        s += " Merlin: {},".format(votingPattern[MERLIN])
        s += " Good: {},".format(votingPattern[GOOD])
        s += " Evil: {},".format(votingPattern[EVIL]) 
        s += " Spy: {}".format(votingPattern[SPY])
        return s

    ###################
    # Special Methods #
    ###################
    def __repr__(self):
        return "Reasoner({})".format(self.game)

    def __str__(self):
        return "Reasoner:{}".format(self.game)

    def border(self, width, style=0):
        if style == 0:
            return "="*(width*17+1) + "\n"
        elif style == 1:
            return "$" + "-"*(width*17-1) + "$\n"
        elif style == 2:
            line = "$" + "-"*16
            for i in range(width-1):
                line += "+" + "-"*16
            return line + "$\n"
        elif style == 3:
            line = "|"
            for i in range(width):
                line += "- -- - -- - -- -|"
            return line + "\n"

    def printModel(self, model):
        width = len(model.scopes)

        # model heading
        s = self.border(width, 1)
        totalWidth = width*17-1
        line = model.title()[:totalWidth]
        line = " "*((totalWidth-len(line))//2) + line
        s += "|" + line + " "*(totalWidth-len(line)) + "|\n"

        # scope sub-headings
        s += self.border(width, 1)
        s += "|"
        for scope in sorted(model.scopes):
            line = "{} knows".format(scope)[:16]
            line = " "*((16-len(line))//2) + line
            s += line + " "*(16-len(line)) + "|"
        s += "\n"

        # loyalty probabilities
        s += self.border(width, 2)
        for player in sorted(self.game.players):
            s += "|"
            for scope in sorted(model.scopes):
                prob = self.playerSpyProbability(player, scope, model)
                line = "{} {:.2%}".format(player.short(), prob)
                s += line[:16] + " "*(16-len(line)) + "|"
            s += "\n"

        # character possibilities
        s += self.border(width, 3)
        for player in sorted(self.game.players):
            s += "|"
            for scope in sorted(model.scopes):
                line = "{} ".format(player.short())
                possible = self.getPossibleCharacters(player, scope, model)
                for character in sorted(self.game.characters):
                    if character in possible:
                        line += character.abbr
                    else:
                        line += " "
                s += line[:16] + " "*(16-len(line)) + "|"
            s += "\n"
        return s

    def printData(self):
        self.refineKnowledge()
        #self.engine.get_kb("k").dump_specific_facts()
        print()
        s = str(self) + "\n"
        width = len(self.game.players)
        s += self.border(width)
        s += self.printModel(CURRENT)
        s += self.border(width)
        for player in sorted(self.game.players):
            for model in sorted(self.models & self.playerModels[player]):
                s += self.printModel(model)
        s += self.border(width)
        print(s.strip())
        self.printAnalysis()

    def printAnalysis(self):
        s = "\nLOYALTY ANALYSIS:"
        for player in sorted(self.game.players):
            s += "\n{} {:.2%} Evil".format(player.short(),
                                       self.playerSpyProbability(player))

        s += "\n\nCHARACTER ANALYSIS:"
        for player in sorted(self.game.players):
            s += "\n{}".format(player.short())
            possible = self.getPossibleCharacters(player, "$_scope")
            for character in sorted(self.game.characters):
                if character in possible:
                    s += " " + str(character)

        s += "\n\nVOTING ANALYISIS:"
        for player in sorted(self.game.players):
            s += self.getSuspicion(player)
        print(s)
        self.game.printMissionHistory()

