import re
import sys

from util import *
from reasoner import Reasoner, CURRENT

files = {}

def _input(text):
    if "in" in files and len(files["in"]) > 0:
        print(text, end="")
        line = files["in"].pop(0).strip()
        print(line)
    else:
        line = input(text)
    if "out" in files:
        files["out"].write(line + "\n")
    return line

#########
# SETUP #
#########
def text_setup(game):
    playerNames = []
    print("\nEnter names of all players in game.")
    print("If there are no more names to add, enter an empty line.")
    print("(type 'rm name' to remove a name from the list)")
    nameKeys = {}
    while True:
        print()
        print(nice(playerNames))
        args = _input("Player name: ").split()
        if len(args) == 0:
            if len(playerNames) < 5:
                print("Not enough players! Minimum is 5.")
            else:
                break
        elif len(args) == 1:
            name, = args
            nameKey = name[:5].lower()
            if len(playerNames) == 10:
                print("Too many players! Maximum is 10.")
            elif not re.match("^[A-Za-z]+$", name):
                print("Invalid name! Must only have letters. /[A-Za-z]+/")
            elif name.lower() in restrictedNames:
                print("Name restricted!")
            elif name in playerNames:
                print("Name already in use!")
            elif nameKey in nameKeys:
                print("Name too similar to {}!".format(nameKeys[nameKey]))
            else:
                playerNames.append(name)
                nameKeys[nameKey] = name
        elif len(args) == 2:
            arg, name = args
            nameKey = name[:5].lower()
            if arg != "rm":
                print("Invalid argument {}!".format(arg))
            elif not name in playerNames:
                print("Name not recognised!")
            else:
                del nameKeys[nameKey]
                playerNames.remove(name)
        else:
            print("Too many arguments!")

    game.setNumPlayers(len(playerNames))

    while True:
        print("\nAvailable game modes: {}, {}".format(RESISTANCE, AVALON))
        gameMode = _input("Enter game mode: ")
        if re.match("^(resistance|r)$", gameMode.lower()):
            game.setGameMode(RESISTANCE)
            print("Playing The Resistance.")
            break
        elif re.match("^(avalon|a)$", gameMode.lower()):
            game.setGameMode(AVALON)
            print("Playing Avalon.")
            break
        else:
            print("Invalid input! /resistance|r|avalon|a/")

    if game.gameMode == AVALON:
        print("\nEnter character cards.")
        print("(type 'clear' to clear character cards)")
        print("(leave line blank when ready)")
        while True:
            game.printCharacterOptions()
            character = _input("Enter character card: ")
            if character == "clear":
                game.clearCharacters()
                continue
            if character == "":
                break
            game.addCharacter(character)

    while True:
        onePlayer = _input("\nAre you playing in this game? ").lower()
        if re.match("^(yes|y)$", onePlayer):
            game.onePlayer = True
            break
        elif re.match("^(no|n)", onePlayer):
            game.onePlayer = False
            break
        else:
            print("Invalid input! /yes|y|no|n/")

    for num, name in enumerate(playerNames):
        if not game.setName(num, name):
            game.crash("Invalid player/name combination: {}/{}".format(num, name))
    if not game.checkNamesReady():
        game.crash("Not enough players have names!")
    if game.onePlayer:
        while True:
            while True:
                print("\nPlayers:", nice(game.players))
                player = _input("Which player are you? ")
                if game.enterMainPlayer(player):
                    break
            while True:
                print(nice(game.characters))
                character = _input("What is your character card? ")
                if game.enterPlayerCharacter(character):
                    break
            print("\nYou are {}.".format(game.player))
            break

    game.reasoner = Reasoner(game)

    if game.onePlayer:
        while True:
            print()
            print(nice(game.reasoner.seenPlayers))
            if game.reasoner.checkReady():
                if len(game.reasoner.seenPlayers) == 0:
                    break
                game.reasoner.finaliseInitialKnowledge()
                break
            game.reasoner.addSeenPlayer(_input("Player you saw: "))

    game.reasoner.generateModels()

########
# PLAY #
########
def setLeader(game, mission):
    while True:
        leader = _input("\nEnter mission {} leader: ".format(mission.num))
        try:
            leader = game.getPlayer(leader)
        except RuntimeError:
            print("Invalid player!")
            continue
        mission.setLeader(leader)
        return

def usePlotCards(game):
    print("\nPlot cards not implemented!")

def suggestPlayers(game, mission):
    print()
    print("\nSuggest {} players.".format(mission.numPlayers))
    print("(type 'clear' to clear suggestions)")
    print()
    while True:
        if len(mission.players) == mission.numPlayers:
            lower, average, upper = game.reasoner.averageTeam(
                                        mission.players)
            print("\nThis team has between {} and {} Evil players.".format(lower,
                                                                         upper))
            print("On average, there are {:.2f} Evil players.".format(average))
            break
        player = _input("Suggest player for {}'s team: ".format(mission.leader))
        if player == "clear":
            mission.clearPlayers()
            continue
        try:
            player = game.getPlayer(player)
        except RuntimeError:
            print("Invalid player!")
            continue
        if player in mission.players:
            print("That has already been suggested!")
            continue
        mission.addPlayer(player)
        print()
        print("Players:", nice(mission.players))

def chooseModel(game, player):
    models = game.reasoner.models & game.reasoner.playerModels[player]
    if len(models) > 0:
        return sorted(models)[0]
    return CURRENT

def enterVotes(game, mission):
    print()
    for player in sorted(game.players):
        model = game.reasoner.hiddenModels[player]
        if not model in game.reasoner.models:
            continue
        lower, average, upper = game.reasoner.averageTeam(mission.players,
                                                          player, model)
        print("{} {} to {} Evil, {:.2f} average (if Loyal)".format(
              player.short(), lower, upper, average))
    print("\nTeam:", nice(mission.players))
    print("Enter players who rejected mission.")
    print("If there are no more names to add, enter an empty line.")
    print("(type 'clear' at any point to clear list of players)")
    rejectingPlayers = set()
    while True:
        print()
        print("Approves:", nice(game.players - rejectingPlayers))
        print("Rejects:", nice(rejectingPlayers))
        player = _input("Rejecting player: ")
        if player == "clear":
            rejectingPlayers = set()
            continue
        elif player == "":
            break
        try:
            player = game.getPlayer(player)
            rejectingPlayers.add(player)
        except RuntimeError:
            print("Invalid player!")

    for player in rejectingPlayers:
        mission.addVote(player, REJECT)
    for player in game.players - rejectingPlayers:
        mission.addVote(player, APPROVE)

def enterResults(game, mission):
    while True:
        numFails = _input("\nEnter number of fail cards: ")
        try:
            numFails = int(numFails)
        except ValueError:
            print("Invalid input!")
            continue
        if numFails < 0:
            print("Can't have a negative number of fails!")
        elif numFails > mission.numPlayers:
            print("Impossible number of fails!")
        elif numFails > game.numEvil:
            print("Impossible number of fails!")
        else:
            print("Mission {} result is {}/{}.".format(mission.players,
                                                       numFails,
                                                       mission.failCondition))
            mission.enterResults(numFails)
            if numFails > 0 and game.onePlayer and OBERON in game.characters:
                if game.player in mission.players and game.player.loyalty == EVIL:
                    while True:
                        playedFail = _input("Did you play fail? ")
                        if re.match("^(yes|y)$", playedFail):
                            game.reasoner.playedFail(mission, True)
                            break
                        elif re.match("^(no|n)$", playedFail):
                            game.reasoner.playedFail(mission, False)
                            break
                        else:
                            print("Invalid input!")
            return

def merlinGuess(game):
    if game.victoryStatus != LOYAL_VICTORY:
        return
    print("\nLoyal victory... unless the spies can work out who Merlin is.")
    print("If the spies guess correctly, they will win the game.")
    while True:
        guess = _input("\nAssassin's guess: ")
        try:
            guess = game.getPlayer(guess)
            break
        except RuntimeError:
            print("Invalid player!")
    while True:
        merlin = _input("\nWho is Merlin? ")
        try:
            merlin = game.getPlayer(merlin)
            break
        except RuntimeError:
            print("Invalid player!")
    game.merlinGuess(merlin, guess)

def text_play(game):
    game.reasoner.printData()
    while game.victoryStatus == INCOMPLETE:
        mission = game.getMission()
        print("\nStarting mission {}.".format(mission.num))
        print("Failure condition: {} cards".format(mission.failCondition))
        setLeader(game, mission)
        #usePlotCards(game)
        while True:
            suggestPlayers(game, mission)
            enterVotes(game, mission)
            if mission.missionApproved():
                game.reasoner.printAnalysis()
                enterResults(game, mission)
                game.reasoner.printData()
                if game.tryVictory():
                    if game.gameMode == AVALON:
                        merlinGuess(game)
                break
            else:
                if game.tryVictory():
                    break
                mission.leader = None
                mission.clearPlayers()
                mission.clearVotes()
                game.reasoner.printAnalysis()
                setLeader(game, mission)
    game.endGame()

#######
# RUN #
#######
yes_pattern = "^(yes|y)$"
file_pattern = "^[A-Za-z][A-Za-z0-9_]*$"
def text_run(game):
    if re.match(yes_pattern, input("Read input from file? ")):
        while True:
            inFile = input("File name: ")
            if re.match(file_pattern, inFile):
                with open('examples/'+inFile, 'r') as f:
                    files["in"] = [line for line in f]
                break
    else:
        inFile = None
    if re.match(yes_pattern, input("Write input to file? ")):
        while True:
            outFile = input("File name: ")
            if outFile == inFile:
                print("Can't use same file as input!")
            elif re.match(file_pattern, outFile):
                print("Writing to file {}.".format(outFile))
                print("This will overwrite file if it already exists.")
                if (yes_pattern, input("Are you sure? ")):
                    with open('examples/'+outFile, 'w') as f:
                        files["out"] = f
                        text_setup(game)
                        text_play(game)
                    return
    text_setup(game)
    text_play(game)

